<?php namespace ProcessWire;
class RockFormsConfig extends ModuleConfig {
  public function getDefaults() {
    return [
      'nettehny' => "web\npassword\n",
    ];
  }
  public function getInputfields() {
    $inputfields = parent::getInputfields();

    $f = $this->modules->get('InputfieldTextarea');
    $f->attr('name', 'nettehny');
    $f->description = 'The module will add honeypot fields with the names specified here. Leave empty to disable honeypots';
    $f->label = 'Honeypot Fields';
    $f->note = 'One per line';
    $inputfields->add($f);

    return $inputfields;
  }
}