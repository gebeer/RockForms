<?php namespace ProcessWire;

use Nette\Forms;
use Nette\Forms\Controls;

/**
 * Simple UiKit renderer for Nette Framework forms.
 * @param ProcessWire\Form $rockForm passes RockForm's Form instance with properties
 */
class UIKitRenderer extends Forms\Rendering\DefaultFormRenderer
{

	public function __construct($rockForm)
	{
        $this->wrappers['controls']['container'] = null;
        $this->wrappers['pair']['container'] = 'div';
        $this->wrappers['error']['container'] = 'div class="uk-alert-danger" uk-alert';
        $this->wrappers['error']['item'] = 'div';

        $this->wrappers['control']['errorcontainer'] = 'div class="uk-alert-danger uk-margin-remove" uk-alert';
        $this->wrappers['control']['erroritem'] = '';
        
        $this->wrappers['group']['container'] = 'div class=uk-margin-small';
        $this->wrappers['group']['label'] = 'div';

        if($rockForm->showLabels) {
          $this->wrappers['label']['container'] = 'div class=uk-form-label';
          $this->wrappers['control']['container'] = 'div class="uk-form-controls uk-margin-small"';
        }
        else {
          $this->wrappers['label']['container'] = 'div class="uk-form-label uk-hidden"';
          $this->wrappers['control']['container'] = 'div class="uk-margin-small"';
        }
	}


	public function render(Forms\Form $form, $mode = NULL)
	{
        foreach($form->getComponents() as $component) {
          switch(true) {
            case $component instanceof \Nette\Forms\Controls\RadioList:
              $component->getControlPrototype()->addClass('uk-radio');
              $component->getSeparatorPrototype()->setName('span class="uk-margin-small-left"');
              $component->getContainerPrototype()->addClass('uk-alert-danger');
              break;
            case $component instanceof \Nette\Forms\Controls\TextInput:
              $component->getControlPrototype()
                ->addClass('uk-input')
                ->addPlaceholder($component->label->getText())
                ;
              break;
            case $component instanceof \Nette\Forms\Controls\TextArea:
              $component->getControlPrototype()
                ->addClass('uk-textarea')
                ->addPlaceholder($component->label->getText())
                ;
              break;
            case $component instanceof \Nette\Forms\Controls\SubmitButton:
              $component->getControlPrototype()->addClass('uk-button');
              break;
          }
        }

		return parent::render($form, $mode);
	}

}